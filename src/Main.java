import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("1: ABC Bank");
        System.out.println("2: Hindi Bank");
        System.out.print("Select Bank to show interest rate:");
        int bank = scanner.nextInt();
        if (bank == 1){
            AbcBank abcBank = new AbcBank();
            abcBank.rateOfInterest();
        }else if (bank == 2){
            HindiBank hindiBank = new HindiBank();
            hindiBank.rateOfInterest();
        }else {
            System.out.println("Bank not existed");
        }
    }
}

abstract class Bank{
    abstract void rateOfInterest();
}

class AbcBank extends Bank{

    @Override
    void rateOfInterest() {
        System.out.println("Interest Rate of ABC Bank = 12.2% / Year");
    }
}
class HindiBank extends Bank{

    @Override
    void rateOfInterest() {
        System.out.println("Interest Rate of Hindi Bank = 11.9% / Year");
    }
}